package fpe

import (
	"crypto/aes"
	"math/rand"
	"testing"
)

func TestGolden(t *testing.T) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		t.Fatal(err)
	}
	maxvalue := uint64(199)
	want := []uint64{
		171, 16, 30, 183, 31, 111, 158, 7, 122, 90, 82, 193, 156, 137, 17, 51, 84, 135,
		184, 186, 169, 38, 155, 197, 72, 85, 5, 21, 160, 45, 48, 101, 119, 102, 65, 130,
		191, 29, 196, 61, 36, 58, 177, 125, 195, 76, 174, 103, 75, 113, 124, 126, 136,
		10, 152, 22, 0, 96, 88, 185, 198, 142, 39, 89, 128, 28, 86, 20, 154, 161, 121,
		50, 114, 168, 54, 106, 162, 170, 143, 93, 129, 8, 68, 139, 79, 153, 60, 95, 53,
		98, 140, 148, 97, 112, 74, 188, 133, 182, 91, 194, 151, 173, 163, 57, 190, 145,
		180, 64, 120, 71, 104, 24, 62, 23, 19, 55, 115, 187, 52, 37, 1, 18, 118, 176,
		109, 150, 33, 34, 94, 2, 199, 132, 100, 15, 138, 6, 46, 146, 159, 78, 63, 127,
		67, 13, 80, 81, 69, 56, 117, 35, 49, 3, 105, 9, 25, 40, 77, 165, 157, 149, 32,
		116, 107, 26, 83, 178, 144, 44, 141, 27, 59, 99, 70, 43, 108, 192, 47, 179,
		175, 41, 87, 123, 147, 92, 42, 172, 110, 167, 12, 73, 181, 189, 11, 4, 134, 66,
		131, 164, 14, 166,
	}
	for plaintext := uint64(0); plaintext <= maxvalue; plaintext++ {
		cipherText := SimpleEncrypt(blockCipher, plaintext, maxvalue)
		recovered := SimpleDecrypt(blockCipher, cipherText, maxvalue)
		if recovered != plaintext {
			t.Fatalf("SimpleDecrypt(SimpleEncrypt(%d, %d)) returned %d", plaintext, maxvalue, recovered)
		}
		if cipherText != want[plaintext] {
			t.Fatalf("SimpleEncrypt(%d, %d) returned %d, want %d", plaintext, maxvalue, cipherText, want[plaintext])
		}
	}
}

var implementations = []struct {
	encrypt, decrypt func(blockCipher BlockCipher, plaintext, maxvalue uint64) uint64
}{
	{
		SimpleEncrypt,
		SimpleDecrypt,
	},
	{
		func(blockCipher BlockCipher, plaintext, maxvalue uint64) uint64 {
			return Encrypt(blockCipher, plaintext, maxvalue, 0, 7)
		},
		func(blockCipher BlockCipher, ciphertext, maxvalue uint64) uint64 {
			return Decrypt(blockCipher, ciphertext, maxvalue, 0, 7)
		},
	},
	{
		func(blockCipher BlockCipher, plaintext, maxvalue uint64) uint64 {
			return Encrypt(blockCipher, plaintext, maxvalue, 5577006791947779410, FeistelRounds)
		},
		func(blockCipher BlockCipher, ciphertext, maxvalue uint64) uint64 {
			return Decrypt(blockCipher, ciphertext, maxvalue, 5577006791947779410, FeistelRounds)
		},
	},
}

func TestConsistency(t *testing.T) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		t.Fatal(err)
	}
	for _, impl := range implementations {
		for maxvalue := uint64(2); maxvalue < 200; maxvalue++ {
			counters := make([]byte, maxvalue+1)
			for plaintext := uint64(0); plaintext <= maxvalue; plaintext++ {
				cipherText := impl.encrypt(blockCipher, plaintext, maxvalue)
				recovered := impl.decrypt(blockCipher, cipherText, maxvalue)
				if recovered != plaintext {
					t.Fatalf("Decrypt(Encrypt(%d, %d)) returned %d", plaintext, maxvalue, recovered)
				}
				if counters[cipherText] != 0 {
					prev := counters[cipherText] - 1
					t.Fatalf("Encrypt(%d, %d) returned %d, which is also result of Encrypt(%d, %d)", plaintext, maxvalue, recovered, prev, maxvalue)
				}
				counters[cipherText] = byte(plaintext + 1)
			}
		}
	}
}

func TestLarge(t *testing.T) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		t.Fatal(err)
	}
	for _, impl := range implementations {
		r := rand.New(rand.NewSource(105))
		for maxvalue := uint64(10000); maxvalue < 1e17; maxvalue *= 10 {
			var inputs []uint64
			for plaintext := uint64(0); plaintext < 5000; plaintext++ {
				inputs = append(inputs, plaintext)
			}
			for plaintext := maxvalue - 100; plaintext <= maxvalue; plaintext++ {
				inputs = append(inputs, plaintext)
			}
			for i := 0; i < 1000; i++ {
				inputs = append(inputs, uint64(r.Intn(int(maxvalue+1))))
			}
			for _, plaintext := range inputs {
				cipherText := impl.encrypt(blockCipher, plaintext, maxvalue)
				recovered := impl.decrypt(blockCipher, cipherText, maxvalue)
				if recovered != plaintext {
					t.Fatalf("Decrypt(Encrypt(%d, %d)) returned %d", plaintext, maxvalue, recovered)
				}
			}
		}
	}
}

func BenchmarkEncrypt_30_100(b *testing.B) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		b.Fatal(err)
	}
	b.ResetTimer()
	b.SetBytes(1)
	for i := 0; i < b.N; i++ {
		SimpleEncrypt(blockCipher, 30, 100)
	}
}

func BenchmarkEncrypt_8bytes(b *testing.B) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		b.Fatal(err)
	}
	b.ResetTimer()
	b.SetBytes(8)
	for i := 0; i < b.N; i++ {
		SimpleEncrypt(blockCipher, 5577006791947779410, 16618917812824593733)
	}
}
