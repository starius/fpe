package phrase

import (
	"crypto/aes"
	"testing"

	"github.com/tyler-smith/go-bip39/wordlists"
)

func TestIDEncoder(t *testing.T) {
	wordsCipher, err := aes.NewCipher([]byte("This is word key"))
	if err != nil {
		t.Fatal(err)
	}
	longCipher, err := aes.NewCipher([]byte("This is long key"))
	if err != nil {
		t.Fatal(err)
	}
	encoder, err := NewIDEncoder(wordsCipher, longCipher, wordlists.English)
	if err != nil {
		t.Fatal(err)
	}
	cases := []struct {
		id   uint64
		text string
		long bool
	}{
		{
			id:   0,
			text: "simple",
			long: false,
		},
		{
			id:   0,
			text: "Gw7hPX1YJxvBU8b1sDBSkQ",
			long: true,
		},
		{
			id:   100000000,
			text: "luggage-photo-yard",
			long: false,
		},
		{
			id:   100000000,
			text: "bkGa_Rm1XusCX8r4KnpLjw",
			long: true,
		},
	}
	for _, tc := range cases {
		text, err := encoder.Encode(tc.id, tc.long)
		if err != nil {
			t.Errorf("encoder.Encode(%d, %v): %v", tc.id, tc.long, err)
		}
		if text != tc.text {
			t.Errorf("encoder.Encode(%d, %v): returned %q, want %q", tc.id, tc.long, text, tc.text)
		}
		id, long, err := encoder.Decode(tc.text)
		if err != nil {
			t.Errorf("encoder.Decode(%q): %v", tc.text, err)
		}
		if id != tc.id || long != tc.long {
			t.Errorf("encoder.Decode(%q): returned (%d, %v), want (%d, %v)", tc.text, id, long, tc.id, tc.long)
		}
	}
}

func TestIDEncoderErrors(t *testing.T) {
	wordsCipher, err := aes.NewCipher([]byte("This is word key"))
	if err != nil {
		t.Fatal(err)
	}
	longCipher, err := aes.NewCipher([]byte("This is long key"))
	if err != nil {
		t.Fatal(err)
	}
	encoder, err := NewIDEncoder(wordsCipher, longCipher, wordlists.English)
	if err != nil {
		t.Fatal(err)
	}
	badInputs := []string{
		"",
		"_",
		"-",
		"bkGa_Rm1XusCX8r4Kn",
		"bkGa",
		"simple-simple-simple-simple-simple-simple-simple-simple-simple-simple-simple-simple",
		"123",
	}
	for _, input := range badInputs {
		_, _, err := encoder.Decode(input)
		if err == nil {
			t.Errorf("encoder.Decode(%q): wanted an error", input)
		}
	}
}
