package phrase

import (
	"crypto/aes"
	"reflect"
	"strings"
	"testing"

	"github.com/tyler-smith/go-bip39/wordlists"
)

func TestDict(t *testing.T) {
	cases := []struct {
		dict    []string
		numbers []uint64
		words   []string
	}{
		{
			dict:    []string{"zero", "one", "two"},
			numbers: []uint64{0, 0},
			words:   []string{"zero", "zero"},
		},
		{
			dict:    []string{"zero", "one", "two"},
			numbers: []uint64{0, 1, 1, 2, 0},
			words:   []string{"zero", "one", "one", "two", "zero"},
		},
	}
	for _, tc := range cases {
		d, err := MakeDict(tc.dict)
		if err != nil {
			t.Errorf("MakeDict(%v): %v", tc.dict, err)
			continue
		}
		words, err := d.ToWords(tc.numbers)
		if err != nil {
			t.Errorf("MakeDict(%v).ToWords(%v): %v", tc.dict, tc.numbers, err)
		} else if !reflect.DeepEqual(words, tc.words) {
			t.Errorf("MakeDict(%v).ToWords(%v) returned %v, want %v", tc.dict, tc.numbers, words, tc.words)
		}
		numbers, err := d.ToNumbers(tc.words)
		if err != nil {
			t.Errorf("MakeDict(%v).ToNumbers(%v): %v", tc.dict, tc.words, err)
		} else if !reflect.DeepEqual(numbers, tc.numbers) {
			t.Errorf("MakeDict(%v).ToNumbers(%v) returned %v, want %v", tc.dict, tc.words, numbers, tc.numbers)
		}
	}
}

func TestDictErrors(t *testing.T) {
	_, err := MakeDict([]string{"zero", "one", "zero"})
	if err == nil {
		t.Errorf("MakeDict(%v): didn't return error", []string{"zero", "one", "zero"})
	}

	d, err := MakeDict([]string{"zero", "one", "two"})
	if err != nil {
		t.Errorf("MakeDict(%v): %v", []string{"zero", "one", "two"}, err)
	}
	_, err = d.ToWords([]uint64{0, 1, 1, 2, 3})
	if err == nil {
		t.Errorf("ToWords: didn't return error")
	}

	_, err = d.ToNumbers([]string{"foo"})
	if err == nil {
		t.Errorf("ToNumbers: didn't return error")
	}
}

func TestGoldenWords(t *testing.T) {
	dict, err := MakeDict(wordlists.English)
	if err != nil {
		t.Fatal(err)
	}
	blockCipher, err := aes.NewCipher([]byte("key for English!"))
	if err != nil {
		t.Fatal(err)
	}
	encrypt, decrypt := MakePRP(blockCipher, 4)
	base := uint64(len(wordlists.English))
	cases := []struct {
		number   uint64
		sentence string
	}{
		{0, "fly"},
		{1, "liar"},
		{100, "include"},
		{1000, "runway"},
		{34424, "load smile"},
		{999999, "mistake before"},
		{1000000, "pride fashion"},
		{55555555, "reveal cram hybrid"},
		{343456564, "increase split repair"},
		{2387432842, "fix shiver moral"},
		{89458954544, "help timber post taste"},
		{556563434792, "garden response matrix thunder"},
		{3294838942733, "scan pony exchange cherry"},
		{29493892840901, "siege matter robot empty shaft"},
		{888384732847777, "manual pistol begin vapor mention"},
		{3294882736243799, "mistake winner ill course eternal"},
		{32423423423423423, "color gloom swarm calm inherit"},
	}
	for _, tc := range cases {
		numbers, err := ToNumbersList(tc.number, base, encrypt)
		if err != nil {
			t.Errorf("ToNumbersList(%d, %d, encrypt): %v", tc.number, base, err)
		}
		words, err := dict.ToWords(numbers)
		if err != nil {
			t.Errorf("ToWords(%v): %v", numbers, err)
		}
		sentence := strings.Join(words, " ")
		if sentence != tc.sentence {
			t.Errorf("number %d: got %q, want %q", tc.number, sentence, tc.sentence)
		}
		numbers2, err := dict.ToNumbers(words)
		if err != nil {
			t.Errorf("ToNumbersList(%v): %v", words, err)
		}
		number2, err := FromNumbersList(numbers2, base, decrypt)
		if err != nil {
			t.Errorf("FromNumbersList(%v, %d, decrypt): %v", numbers2, base, err)
		}
		if number2 != tc.number {
			t.Errorf("number %d: got back %d", tc.number, number2)
		}
	}
}
