package phrase

import (
	"crypto/aes"
	"encoding/json"
	"reflect"
	"testing"
)

func nop(input, maxvalue uint64) uint64 {
	return input
}

func TestNumbersList(t *testing.T) {
	cases := []struct {
		number, base uint64
		list         []uint64
	}{
		// Unary numeral system.
		{
			number: 0,
			base:   1,
			list:   []uint64{0},
		},
		{
			number: 1,
			base:   1,
			list:   []uint64{0, 0},
		},
		{
			number: 2,
			base:   1,
			list:   []uint64{0, 0, 0},
		},
		{
			number: 10,
			base:   1,
			list:   []uint64{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},

		// Base: 2. Note it is not binary numeral system.
		{
			number: 0,
			base:   2,
			list:   []uint64{0},
		},
		{
			number: 1,
			base:   2,
			list:   []uint64{1},
		},
		{
			number: 2,
			base:   2,
			list:   []uint64{0, 0},
		},
		{
			number: 3,
			base:   2,
			list:   []uint64{0, 1},
		},
		{
			number: 4,
			base:   2,
			list:   []uint64{1, 0},
		},
		{
			number: 5,
			base:   2,
			list:   []uint64{1, 1},
		},
		{
			number: 6,
			base:   2,
			list:   []uint64{0, 0, 0},
		},
		{
			number: 7,
			base:   2,
			list:   []uint64{0, 0, 1},
		},
		{
			number: 8,
			base:   2,
			list:   []uint64{0, 1, 0},
		},
		{
			number: 9,
			base:   2,
			list:   []uint64{0, 1, 1},
		},

		// Base: 10. Note it is not decimal numeral system.
		{
			number: 0,
			base:   10,
			list:   []uint64{0},
		},
		{
			number: 1,
			base:   10,
			list:   []uint64{1},
		},
		{
			number: 9,
			base:   10,
			list:   []uint64{9},
		},
		{
			number: 10,
			base:   10,
			list:   []uint64{0, 0},
		},
		{
			number: 11,
			base:   10,
			list:   []uint64{0, 1},
		},
		{
			number: 12,
			base:   10,
			list:   []uint64{0, 2},
		},
		{
			number: 19,
			base:   10,
			list:   []uint64{0, 9},
		},
		{
			number: 20,
			base:   10,
			list:   []uint64{1, 0},
		},
		{
			number: 21,
			base:   10,
			list:   []uint64{1, 1},
		},
		{
			number: 22,
			base:   10,
			list:   []uint64{1, 2},
		},
		{
			number: 109,
			base:   10,
			list:   []uint64{9, 9},
		},
		{
			number: 110,
			base:   10,
			list:   []uint64{0, 0, 0},
		},
		{
			number: 111,
			base:   10,
			list:   []uint64{0, 0, 1},
		},

		// Edge cases.
		{
			number: 1<<64 - 10,
			base:   1<<64 - 5,
			list:   []uint64{1<<64 - 10},
		},
	}
	for _, tc := range cases {
		list, err := ToNumbersList(tc.number, tc.base, nop)
		if err != nil {
			t.Errorf("case %v: ToNumbersList(%d, %d, nop): %v", tc, tc.number, tc.base, err)
		} else if !reflect.DeepEqual(list, tc.list) {
			t.Errorf("case %v: ToNumbersList(%d, %d, nop) returned %v, want %v", tc, tc.number, tc.base, list, tc.list)
		}
		number, err := FromNumbersList(tc.list, tc.base, nop)
		if err != nil {
			t.Errorf("case %v: FromNumbersList(%v, %d, nop): %v", tc, tc.list, tc.base, err)
		} else if number != tc.number {
			t.Errorf("case %v: FromNumbersList(%v, %d, nop) returned %d, want %d", tc, tc.list, tc.base, number, tc.number)
		}
	}
}

func TestToNumbersListOverflow(t *testing.T) {
	cases := []struct {
		number, base uint64
	}{
		{
			number: 1<<64 - 5,
			base:   1<<64 - 10,
		},
	}
	for _, tc := range cases {
		_, err := ToNumbersList(tc.number, tc.base, nop)
		if err != ErrOverflow {
			t.Errorf("case %v: ToNumbersList(%d, %d, nop): want overflow", tc, tc.number, tc.base)
		}
	}
}

func TestFromNumbersListOverflow(t *testing.T) {
	cases := []struct {
		base uint64
		list []uint64
	}{
		{
			base: 1<<64 - 10,
			list: []uint64{0, 0},
		},
		{
			base: 1<<64 - 10,
			list: []uint64{1, 1, 1},
		},
		{
			base: 10,
			list: []uint64{1, 8, 4, 4, 6, 7, 4, 4, 0, 7, 3, 7, 0, 9, 5, 5, 1, 6, 1, 6},
		},
		{
			base: 10,
			list: []uint64{10},
		},
	}
	for _, tc := range cases {
		_, err := FromNumbersList(tc.list, tc.base, nop)
		if err != ErrOverflow {
			t.Errorf("case %v: FromNumbersList(%v, %d, nop): want overflow", tc, tc.list, tc.base)
		}
	}
}

func BenchmarkNumbersNop(b *testing.B) {
	base := uint64(1626)
	b.SetBytes(4)
	for i := 0; i < b.N; i++ {
		number := uint64(i)
		list, err := ToNumbersList(number, base, nop)
		if err != nil {
			b.Fatalf("ToNumbersList(%d, %d, nop): %v", number, base, err)
		}
		number2, err := FromNumbersList(list, base, nop)
		if err != nil {
			b.Fatalf("FromNumbersList(%v, %d, nop): %v", list, base, err)
		}
		if number2 != number {
			b.Fatalf("FromNumbersList(ToNumbersList(%d, %d, nop)) returned %d", number, base, number2)
		}
	}
}

func TestRealCrypto(t *testing.T) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		t.Fatal(err)
	}
	encrypt, decrypt := MakePRP(blockCipher, 4)
	base := uint64(162)
	for number := uint64(0); number < 1000000; number++ {
		list, err := ToNumbersList(number, base, encrypt)
		if err != nil {
			t.Fatalf("ToNumbersList(%d, %d, encrypt): %v", number, base, err)
		}
		number2, err := FromNumbersList(list, base, decrypt)
		if err != nil {
			t.Fatalf("FromNumbersList(%v, %d, decrypt): %v", list, base, err)
		}
		if number2 != number {
			t.Fatalf("FromNumbersList(ToNumbersList(%d, %d, decrypt)) returned %d", number, base, number2)
		}
	}
}

func BenchmarkNumbersRealCrypto(b *testing.B) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		b.Fatal(err)
	}
	encrypt, decrypt := MakePRP(blockCipher, 4)
	base := uint64(1626)
	b.SetBytes(4)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		number := uint64(i)
		list, err := ToNumbersList(number, base, encrypt)
		if err != nil {
			b.Fatalf("ToNumbersList(%d, %d, nop): %v", number, base, err)
		}
		number2, err := FromNumbersList(list, base, decrypt)
		if err != nil {
			b.Fatalf("FromNumbersList(%v, %d, nop): %v", list, base, err)
		}
		if number2 != number {
			b.Fatalf("FromNumbersList(ToNumbersList(%d, %d, nop)) returned %d", number, base, number2)
		}
	}
}

func TestGolden(t *testing.T) {
	key := make([]byte, 16)
	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		t.Fatal(err)
	}
	encrypt, decrypt := MakePRP(blockCipher, 4)
	base := uint64(5)
	wantJSON := `[
		[2],[3],[0],[1],[4],

		[1,2],[1,1],[4,1],[3,2],[1,4],[3,0],[2,1],[0,4],[0,2],[0,3],[3,1],[4,0],
		[2,2],[2,0],[3,3],[1,3],[1,0],[2,4],[4,3],[4,4],[4,2],[2,3],[0,1],[3,4],
		[0,0],

		[1,1,4],[0,0,2],[3,1,4],[1,1,0],[3,2,3],[0,1,1],[3,0,0],[1,0,0],[4,1,1],
		[0,4,0],[0,4,3],[3,3,2],[2,4,4],[4,0,4],[1,3,0],[4,3,4],[0,0,4],[3,0,1],
		[2,0,4],[4,2,2],[4,1,4],[0,4,4],[4,3,2],[3,0,3],[0,2,1],[0,2,0],[3,3,3],
		[4,1,0],[1,2,1],[4,4,3],[4,1,2],[2,4,3],[4,3,0],[3,2,2],[0,0,1],[3,1,3],
		[2,3,4],[2,3,1],[2,0,0],[4,2,3],[4,4,4],[2,2,1],[4,0,0],[4,4,1],[0,2,3],
		[2,3,3],[1,2,2],[2,1,4],[3,0,2],[2,2,4],[1,1,1],[1,1,3],[1,2,3],[1,4,4],
		[0,3,2],[2,1,1],[1,3,2],[1,3,3],[4,4,0],[0,4,1],[3,4,3],[3,1,2],[2,4,1],
		[1,4,0],[1,4,2],[2,4,2],[4,0,3],[0,1,3],[1,0,1],[1,0,3],[3,2,4],[3,3,1],
		[1,2,0],[3,2,1],[3,2,0],[1,2,4],[1,4,3],[4,0,1],[2,1,3],[4,2,0],[4,3,3],
		[3,3,4],[2,3,0],[0,3,0],[1,0,4],[0,2,4],[3,0,4],[2,0,2],[4,3,1],[2,0,3],
		[2,3,2],[1,1,2],[2,1,0],[3,4,4],[4,0,2],[2,0,1],[0,2,2],[2,2,2],[1,0,2],
		[0,1,0],[4,2,4],[3,4,1],[0,0,0],[2,4,0],[1,4,1],[2,2,3],[2,2,0],[3,1,0],
		[0,4,2],[3,3,0],[0,3,4],[1,3,1],[3,4,0],[0,3,3],[0,1,4],[1,3,4],[3,4,2],
		[0,0,3],[2,1,2],[3,1,1],[0,1,2],[4,1,3],[0,3,1],[4,2,1],[4,4,2],

		[0,2,0,4],[0,4,3,3],[4,4,1,3],[0,2,2,4],[4,3,3,2],[4,4,3,1],[2,0,0,4],
		[1,4,4,4],[4,2,0,0],[1,4,4,3],[3,4,2,3],[2,3,1,4],[0,0,1,2],[1,3,0,4],
		[3,4,2,1],[1,0,3,2],[0,4,0,4],[4,4,3,3],[3,1,0,2],[3,0,3,1],[4,0,2,1],
		[3,3,0,0],[3,1,3,4],[3,3,3,1],[1,3,1,2],[1,0,2,3],[4,0,3,2],[0,3,1,4],
		[2,4,1,4],[2,1,0,4],[4,4,3,0],[3,4,4,0],[4,3,4,3],[3,2,4,3],[3,4,3,1],
		[3,3,4,1],[0,0,2,0],[3,0,4,4],[0,4,2,0],[3,3,0,4],[0,4,2,1],[3,4,0,2],
		[2,4,2,1],[4,3,4,0],[3,4,1,3]
	]`
	want := [][]uint64{}
	if err := json.Unmarshal([]byte(wantJSON), &want); err != nil {
		t.Fatal(err)
	}
	for number := uint64(0); number < 200; number++ {
		list, err := ToNumbersList(number, base, encrypt)
		if err != nil {
			t.Fatalf("ToNumbersList(%d, %d, encrypt): %v", number, base, err)
		}
		number2, err := FromNumbersList(list, base, decrypt)
		if err != nil {
			t.Fatalf("FromNumbersList(%v, %d, decrypt): %v", list, base, err)
		}
		if number2 != number {
			t.Fatalf("FromNumbersList(ToNumbersList(%d, %d, decrypt)) returned %d", number, base, number2)
		}
		if !reflect.DeepEqual(list, want[number]) {
			t.Fatalf("ToNumbersList(%d, %d, decrypt) returned %v, want %v", number, base, list, want[number])
		}
	}
}
